﻿using System;
using System.Data;
using System.Diagnostics;
using System.Linq;
using MySql.Data.MySqlClient;

namespace Services
{
    public class tORM
    {
        private string _commandText;
        private readonly string _connectString;
        private MySqlCommand _myCommand;
        private MySqlDataReader _myDataReader;
        private readonly MySqlConnection _myConnection;
        public DataTable DTable = new DataTable();

        public enum DBForConnection
        {
            Anton,
            Leonid
        }

        public int Price
        {
            get
            {
                if (DTable != null)
                {
                    if (DTable.TableName == "b_sale_order")
                        return 17;
                }
                return -1;
            }
        }

        public tORM(DBForConnection db)
        {
            switch (db)
            {
                case DBForConnection.Anton:
                    _connectString = "Database=beta;Data Source=192.168.0.152;User Id=root;Password=123456;Allow Zero Datetime=true";
                    break;
                case DBForConnection.Leonid:
                    _connectString = "Database=bitlh;Data Source=192.168.0.151;User Id=root;Password=bitbit;Allow Zero Datetime=true";
                    break;
            }
            _myConnection = new MySqlConnection(_connectString);
            try
            {
                _myConnection.Open();
            }
            catch (Exception e)
            {
                Debug.WriteLine("Ошибка подключения к БД: " + e.Message);
            }   
        }

        public void Close()
        {
            _myConnection.Close();
        }

        public DataTable SetTable(string tableName)
        {
            _commandText = "SELECT * FROM " + tableName;
            _myCommand = new MySqlCommand(_commandText, _myConnection);
            _myDataReader = _myCommand.ExecuteReader();
            DTable.Load(_myDataReader);
            return DTable;
        }

        //@todo: Нужен рефакторинг запроса
        public int GetProductWithCountMore1Item()
        {
            _commandText = "SELECT id " + 
                           "FROM beta.b_catalog_product " + 
                           "WHERE timestamp_x <= curdate() AND " + 
	                             "timestamp_x >= curdate() - INTERVAL 15 DAY AND " +
	                             "quantity > 1 " + 
                           "ORDER BY timestamp_x DESC " + 
                           "LIMIT 1;";
            _myCommand = new MySqlCommand(_commandText, _myConnection);
            _myDataReader = _myCommand.ExecuteReader();
            DTable.Load(_myDataReader);
            return (int) DTable.Rows[0].ItemArray.First();
        }
    }
}
