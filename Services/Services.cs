﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Diagnostics;
using HtmlAgilityPack;

namespace BITTests
{
    public static class ServiceAction
    {
        private static readonly HtmlWeb WebGet = new HtmlWeb();
        private static readonly Random Random = new Random();

        private const string AlphabetEn = "abcdefghijklmnopqrstufvwxyz";
        private const string AlphabetRus = "абвгдеёжзийклмнопрстуфХЦЧШЩЪЫЬЭЮЯ";
        private const string Symbols = "~!@#$%^&*()_+";
        private static string _host = "http://bit.lh";

        public const string PathToScreenshots = @"Z:\Fileshare\TestScreenshots\";
        //public const int DiscountForRegistration = 1000;

        public static string Host
        {
            get { return _host; }
            set { _host = value; }
        }

        public static int OrderId
        {
            get
            {
                return File.ReadAllText("Orderid.txt").Any() ? Convert.ToInt32(File.ReadAllText("Orderid.txt")) : 0;
            }
        }

        public static string RandomEmail
        {
            get { return "bit." + Random.Next() + "@mfsa.ru"; }
        }

        public static string RandomPassword
        {
            get
            {
                var pass = "";
                for (int i = 0; i < 2; i++)
                {
                    pass += AlphabetEn[Random.Next(AlphabetEn.Length)];
                    pass += AlphabetRus[Random.Next(AlphabetRus.Length)];
                    pass += Symbols[Random.Next(Symbols.Length)];
                    pass += Convert.ToString(Random.Next(9));
                }
                return pass;
            }
        }

        public static string GetRandomCatalogPage()
        {
            var sitemap = WebGet.Load(Host + "/sitemap/");
            var childNodes = sitemap.DocumentNode.SelectSingleNode("//*[@class='sitemapmenu']")
                                    .SelectNodes("//a").Where(x =>
                                         x.GetAttributeValue("href", "empty").Contains("catalog/women") ||
                                         x.GetAttributeValue("href", "empty").Contains("catalog/men") ||
                                         x.GetAttributeValue("href", "empty").Contains("catalog/children/girls") ||
                                         x.GetAttributeValue("href", "empty").Contains("catalog/children/boys"));
            return Host + childNodes.ElementAt(Random.Next(childNodes.Count() - 1)).GetAttributeValue("href", "");
        }

        public static string GetRandomBrandPage()
        {
            string brandUrl = "";
            do
            {
                try
                {
                    var siteMap = WebGet.Load(Host + "/brands/");
                    var childNodes = siteMap.DocumentNode.SelectSingleNode("//*[@class='contbrandlist_closer']")
                                        .SelectNodes("//a").Where(x =>
                                            x.GetAttributeValue("href", "empty").Contains("brands/") &&
                                            !x.GetAttributeValue("href", "empty").Contains("?") &&
                                            !Regex.IsMatch(x.GetAttributeValue("href", "empty"), @"brands/$"));
                    brandUrl = Host + childNodes.ElementAt(Random.Next(childNodes.Count() - 1)).GetAttributeValue("href", "");
                }
                catch (System.Net.WebException sException)
                {
                    Debug.WriteLine(sException.Message + "(херня...)");
                }
                catch (Exception e)
                {
                    Debug.WriteLine(e.Message);
                }
            } while (WebGet.Load(brandUrl).DocumentNode.SelectNodes("//*[@class='product_one_parent']").Count == 0);
            return brandUrl;
        }

        public static string GetRandomProductPage()
        {
            var linksOfProducts = WebGet.Load(Host + "/catalog/#page=" + Random.Next(50)).DocumentNode.SelectSingleNode("//*[@class='product_list']")
                                        .SelectNodes("//a[@class='preload']");
            return Host + linksOfProducts[Random.Next(linksOfProducts.Count)].GetAttributeValue("href", "empty");
        }

        public static string GetGluedLine(List<string> list)
        {
            string result = list.Aggregate("", (current, t) => current + (t + ", "));
            return result.Remove(result.Length - 2);
        }
    }
}