﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BITTests.Pages.Elements
{
    public class FilterBlock :CommonLayer
    {
        public FilterBlock(IWebDriver browser) : base(browser)
        {
        }

        [FindsBy(How = How.Id, Using = "filteron_brand")]
        [CacheLookup]
        public IWebElement FilterOnBrand;

        [FindsBy(How = How.Id, Using = "filteron_size")]
        [CacheLookup]
        public IWebElement FilterOnSize;

        [FindsBy(How = How.Id, Using = "filteron_colormain")]
        [CacheLookup]
        public IWebElement FilterOnColor;

        [FindsBy(How = How.Id, Using = "filteron_style")]
        [CacheLookup]
        public IWebElement FilterOnStyle;

        [FindsBy(How = How.Id, Using = "filteron_material")]
        [CacheLookup]
        public IWebElement FilterOnMaterial;

        [FindsBy(How = How.Id, Using = "filteron_season")]
        [CacheLookup]
        public IWebElement FilterOnSeason;

        [FindsBy(How = How.Id, Using = "filteron_collection")]
        [CacheLookup]
        public IWebElement FilterOnCollection;

        [FindsBy(How = How.Id, Using = "price-range")]
        [CacheLookup]
        public IWebElement FilterOnPrice;

        [FindsBy(How = How.Id, Using = "price-range-nums")]
        [CacheLookup]
        public IWebElement PriceRangeNums;
        
        /// <summary>
        /// Рандомный выбор бренда
        /// </summary>
        public void FilterByBrand()
        {
            JsAction("$('#filteron_brand a span').mouseover()");
            JsAction("cnt = $('#filteron_brand .jscrolled li').size(); " +
                     "$('#filteron_brand ul.jscrolled > li:eq(' + (Math.floor(Math.random()*(cnt))) + ') a').click();");
        }

        public void FilterByBrand(string brandName)
        {
            JsAction("$('#filteron_brand a span').mouseover()");
            JsAction("$('#filteron_brand ul.jscrolled > li > a[rel=\'" + brandName.Replace(" ", "").ToLower() + "\']').click()");
        }

        /// <summary>
        /// Рандомный выбор размера
        /// </summary>
        public void FilterBySize()
        {
            JsAction("$('#filteron_size a span').mouseover()");
            JsAction("cnt = $('#filteron_size .jscrolled li').size(); " +
                     "$('#filteron_size ul.jscrolled > li:eq(' + (Math.floor(Math.random()*(cnt))) + ') a').click();");
        }

        public void FilterBySize(string size)
        {
            
        }

        /// <summary>
        /// Рандомный выбор стиля
        /// </summary>
        public void FilterByStyle()
        {
            JsAction("$('#filteron_style a span').mouseover()");
            JsAction("cnt = $('#filteron_style .jscrolled li').size(); " +
                     "$('#filteron_style ul.jscrolled > li:eq(' + (Math.floor(Math.random()*(cnt))) + ') a').click();");
        }

        public void FilterByStyle(string style)
        {

        }

        /// <summary>
        /// Рандомный выбор материала
        /// </summary>
        public void FilterByMaterial()
        {
            JsAction("$('#filteron_material a span').mouseover()");
            JsAction("cnt = $('#filteron_material .jscrolled li').size(); " +
                     "$('#filteron_material ul.jscrolled > li:eq(' + (Math.floor(Math.random()*(cnt))) + ') a').click();");
        }

        public void FilterByMaterial(string material)
        {

        }

        /// <summary>
        /// Рандомный выбор сезона
        /// </summary>
        public void FilterBySeason()
        {
            JsAction("$('#filteron_season a span').mouseover()");
            JsAction("cnt = $('#filteron_season .jscrolled li').size(); " +
                     "$('#filteron_season ul.jscrolled > li:eq(' + (Math.floor(Math.random()*(cnt))) + ') a').click();");
        }

        public void FilterBySeason(string season)
        {

        }

        /// <summary>
        /// Рандомный выбор коллекции
        /// </summary>
        public void FilterByCollection()
        {
            JsAction("$('#filteron_collection a span').mouseover()");
            JsAction("cnt = $('#filteron_collection .jscrolled li').size(); " +
                     "$('#filteron_collection ul.jscrolled > li:eq(' + (Math.floor(Math.random()*(cnt))) + ') a').click();");
        }

        public void FilterByCollection(string season)
        {

        }

        /// <summary>
        /// Фильтрует по наименьшему значению диапазона. Т.е. если диапазон от 1000 до 10000 руб, то фильтрует от 1000 до 1000
        /// </summary>
        public void FilterByPrice()
        {
            var minPrice = (int)Convert.ToUInt32(Regex.Match(PriceRangeNums.FindElement(By.ClassName("leftnum")).Text, @"\d+").Value);
            browser.Navigate().GoToUrl(browser.Url + "#category=" + Regex.Match(browser.Url, @"(\w+)/$").Value.Replace("/", "") + ";sort=color_asc;price=0," + minPrice);
            SleepOn(2);
            WaitForJQuery();
        }

        /// <summary>
        /// Выбор заданного ценового диапазона
        /// </summary>
        /// <param name="minPrice">минимальное значение цены</param>
        /// <param name="maxPrice">максимальное значение цены</param>
        public void FilterByPrice(int minPrice, int maxPrice)
        {
            browser.Navigate().GoToUrl(browser.Url + "#category=" + Regex.Match(browser.Url, @"(\w+)/$").Value.Replace("/", "") + ";sort=color_asc;price=" + minPrice + "," + maxPrice);
            SleepOn(2);
            WaitForJQuery();
        }
    }
}
