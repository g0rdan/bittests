﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BITTests.Pages.Elements
{
    public class SortBlock : CommonLayer
    {
        public SortBlock(IWebDriver browser) : base(browser)
        {
        }

        public IWebElement DefaultSortElement
        {
            get { return FindElement(browser, By.ClassName("color_increase")); }
        }

        public ReadOnlyCollection<IWebElement> DefaultSortElements
        {
            get { return FindElements(browser, By.ClassName("color_increase")); }
        }

        public IWebElement NewSortElement
        {
            get { return FindElement(browser, By.ClassName("date_decrease")); }
        }

        public ReadOnlyCollection<IWebElement> NewSortElements
        {
            get { return FindElements(browser, By.ClassName("date_decrease")); }
        }

        public IWebElement PriceSortElementByASC
        {
            get { return FindElement(browser, By.ClassName("price_increase")); }
        }

        public IWebElement PriceSortElementByDESC
        {
            get { return FindElement(browser, By.ClassName("price_decrease")); }
        }

        public ReadOnlyCollection<IWebElement> PriceSortElementsByASC
        {
            get { return FindElements(browser, By.ClassName("price_increase")); }
        }

        public ReadOnlyCollection<IWebElement> PriceSortElementsByDESC
        {
            get { return FindElements(browser, By.ClassName("price_decrease")); }
        }

        public IWebElement PopularSortElement
        {
            get { return FindElement(browser, By.ClassName("rating_decrease")); }
        }

        public ReadOnlyCollection<IWebElement> PopularSortElements
        {
            get { return FindElements(browser, By.ClassName("rating_decrease")); }
        }

        public IWebElement ViewCountSortElement
        {
            get { return FindElement(browser, By.ClassName("view_decrease")); }
        }

        public ReadOnlyCollection<IWebElement> ViewCountSortElements
        {
            get { return FindElements(browser, By.ClassName("view_decrease")); }
        }

        public void SortByDefault()
        {
            DefaultSortElement.Click();
            WaitForJQuery();
        }

        public void SortByNew()
        {
            NewSortElement.Click();
            WaitForJQuery();
        }

        public void SortByPrice(SortType sortType)
        {
            switch (sortType)
            {
                case SortType.ASC:
                    JsAction("$('.price_increase')[0].click()");
                    break;
                case SortType.DESC:
                    JsAction("$('.price_decrease')[0].click()");
                    break;
            }
        }

        public void SortByPopular()
        {
            PopularSortElement.Click();
            WaitForJQuery();
        }

        public void SortByViewCount()
        {
            ViewCountSortElement.Click();
            WaitForJQuery();
        }
    }
}
