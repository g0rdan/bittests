﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BITTests.Pages.Elements
{
    public class HeaderBlock : CommonLayer
    {
        public HeaderBlock(IWebDriver browser) : base(browser)
        {
        }

        public enum SocNetwork
        {
            Vk,
            Facebook,
            Twitter,
            Google
        }

        [FindsBy(How = How.Id, Using = "log_mail")]
        [CacheLookup]
        public IWebElement LoginEmailInput;

        [FindsBy(How = How.Id, Using = "log_pass")]
        [CacheLookup]
        public IWebElement LoginPasswordInput;

        [FindsBy(How = How.Name, Using = "Login")]
        [CacheLookup]
        public IWebElement LoginSubmitButton;

        [FindsBy(How = How.Id, Using = "reg_mail")]
        [CacheLookup]
        public IWebElement RegEmailInput;

        [FindsBy(How = How.Id, Using = "reg_pass")]
        [CacheLookup]
        public IWebElement RegPasswordInput;

        [FindsBy(How = How.Id, Using = "reg_loyalty_cart")]
        [CacheLookup]
        public IWebElement LoyalityInput;

        [FindsBy(How = How.Name, Using = "register_submit_button")]
        [CacheLookup]
        public IWebElement RegSubmitButton;

        [FindsBy(How = How.Id, Using = "reg")]
        [CacheLookup]
        public IWebElement RegLink;

        [FindsBy(How = How.ClassName, Using = "socentervkontakte")]
        [CacheLookup]
        public IWebElement VkLoginLink;

        [FindsBy(How = How.ClassName, Using = "socentertwitter")]
        [CacheLookup]
        public IWebElement TwitterLoginLink;

        [FindsBy(How = How.ClassName, Using = "socenterfacebook")]
        [CacheLookup]
        public IWebElement FacebookLoginLink;

        [FindsBy(How = How.ClassName, Using = "socentergoogle")]
        [CacheLookup]
        public IWebElement GoogleLoginLink;

        public void SignIn(string email, string password)
        {
            OpenRegAuthBlock();
            LoginEmailInput.SendKeys(email);
            LoginPasswordInput.SendKeys(password);
            LoginSubmitButton.Click();
            Thread.Sleep(500);
        }

        public void SignIn(SocNetwork socNetowrk)
        {
            OpenRegAuthBlock();
            switch (socNetowrk)
            {
                case SocNetwork.Vk:
                    VkLoginLink.Click();
                    browser.SwitchTo().Window(browser.WindowHandles[1]);
                    FindElement(browser, By.Name("email")).SendKeys("gordan.ru@gmail.com");
                    FindElement(browser, By.Name("pass")).SendKeys("v77753360xlM4G1J0");
                    FindElement(browser, By.Id("install_allow")).Click();
                    break;
                case SocNetwork.Facebook:
                    FacebookLoginLink.Click();
                    break;
                case SocNetwork.Twitter:
                    TwitterLoginLink.Click();
                    break;
                case SocNetwork.Google:
                    GoogleLoginLink.Click();
                    break;
            }
            browser.SwitchTo().Window(browser.WindowHandles[0]);
        }

        public void SignUp(string email)
        {
            OpenRegAuthBlock();
            RegEmailInput.SendKeys(email);
            RegSubmitButton.Click();
        }

        public void SignUp(string email, string password)
        {
            OpenRegAuthBlock();
            RegEmailInput.SendKeys(email);
            RegPasswordInput.SendKeys(password);
            RegSubmitButton.Click();
        }

        public void SignUp(string email, string password, string loyalityCard)
        {
            OpenRegAuthBlock();
            RegEmailInput.SendKeys(email);
            RegPasswordInput.SendKeys(password);
            LoyalityInput.SendKeys(loyalityCard);
            RegSubmitButton.Click();
        }

        private void OpenRegAuthBlock()
        {
            JsAction("$('#reg').click()");
            Thread.Sleep(3000);
        }
    }
}
