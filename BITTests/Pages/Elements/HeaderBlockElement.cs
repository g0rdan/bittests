﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BITTests.Pages.Elements
{
    class HeaderBlockElement : CommonLayer
    {
        public enum SocNetwork
        {
            VK,
            Facebook,
            Twitter,
            Google
        };

        [FindsBy(How = How.Id, Using = "log_mail")]
        [CacheLookup]
        public IWebElement LoginEmailInput;

        [FindsBy(How = How.Id, Using = "log_pass")]
        [CacheLookup]
        public IWebElement LoginPasswordInput;

        [FindsBy(How = How.Name, Using = "Login")]
        [CacheLookup]
        public IWebElement LoginSubmitButton;

        [FindsBy(How = How.Id, Using = "reg_mail")]
        [CacheLookup]
        public IWebElement RegEmailInput;

        [FindsBy(How = How.Id, Using = "reg_pass")]
        [CacheLookup]
        public IWebElement RegPasswordInput;

        [FindsBy(How = How.Name, Using = "register_submit_button")]
        [CacheLookup]
        public IWebElement RegSubmitButton;

        [FindsBy(How = How.Id, Using = "reg")]
        [CacheLookup]
        public IWebElement RegLink;

        [FindsBy(How = How.Id, Using = "reg_loyalty_cart")]
        [CacheLookup]
        public IWebElement LoyalityInput;
        
        public HeaderBlockElement(IWebDriver browser) : base(browser)
        {

        }

        public void SignIn(string email, string password)
        {
            OpenRegAuthBlock();
            LoginEmailInput.SendKeys(email);
            LoginPasswordInput.SendKeys(password);
            LoginSubmitButton.Click();
        }

        public void SignIn(SocNetwork socNetwork)
        {
            switch (socNetwork)
            {
                case SocNetwork.VK:
                    
                    break;
                case SocNetwork.Facebook:
                    
                    break;
                case SocNetwork.Twitter:

                    break;
                case SocNetwork.Google:

                    break;
            }
        }

        public void SignUp(string email)
        {
            OpenRegAuthBlock();
            RegEmailInput.SendKeys(email);
            RegPasswordInput.SendKeys("");
            RegSubmitButton.Click();
        }

        public void SignUp(string email, string password)
        {
            OpenRegAuthBlock();
            RegEmailInput.SendKeys(email);
            RegPasswordInput.SendKeys(password);
            RegSubmitButton.Click();
        }

        public void SignUp(string email, string password, string loyalityNumber)
        {
            OpenRegAuthBlock();
            RegEmailInput.SendKeys(email);
            RegPasswordInput.SendKeys(password);
            LoyalityInput.SendKeys(loyalityNumber);
            RegSubmitButton.Click();
        }

        private void OpenRegAuthBlock()
        {
            jsAction("$('#reg').click()");
            Thread.Sleep(3000);
        }
    }
}
