﻿using System;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using OpenQA.Selenium;
using System.IO;
using OpenQA.Selenium.Support.PageObjects;

namespace BITTests.Pages
{
    public class Page :CommonLayer
    {
        protected Page(IWebDriver browser) : base(browser) { }
        protected Page(IWebDriver browser, string url) : base(browser) { }
        protected Page(IWebDriver browser, bool popupClose) : base(browser) { }
        protected Page(IWebDriver browser, string url, bool popupClose) : base(browser) { }

        #region "Basic methods"
        /// <summary>
        /// Метод проверки страницы на принадлежность к гендерному типу
        /// </summary>
        /// <returns>Строка "men", "women" или "unknow"</returns>
        public string GetGenderType()
        {
            if (browser.Url.Contains("/women"))
            {
                return "women";
            }
            return browser.Url.Contains("/men") ? "men" : "unknow";
        }

        /// <summary>
        /// Проверяет завершился ли процесс заказа (появление thank you page)
        /// </summary>
        public bool IsOrderCompletePage
        {
            get { return browser.Url.Contains("order.php?"); }
        }

        /// <summary>
        /// Закрывает PopUp окно (регистрация, социальная всплываха)
        /// </summary>
        /// <param name="platformMethod">Закрываем через JS или Selenium</param>
        public void ClosePopupWindow(PlatformMethod platformMethod = PlatformMethod.JS)
        {
            switch (platformMethod)
            {
                case PlatformMethod.JS:
                    JsAction("if($('.a-form-close-button').size() > 0) $('.a-form-close-button').click()");
                    break;
                case PlatformMethod.Selenium:
                    if (IsElementPresent(By.ClassName("a-form-close-button")))
                        FindElement(browser, By.ClassName("a-form-close-button")).Click();
                    break;
            }
        }
        
        /// <summary>
        /// Закрывает окно успешной регистрации
        /// </summary>
        public void CloseSuccessAuthWindow()
        {
            if (jsExec != null)
            {
                JsAction("$('.qform_close a').click()");
            }
        }

        /// <summary>
        /// Это аутлет сайт?
        /// </summary>
        public bool IsOutlet()
        {
            return browser.Url.Contains("outlet");
        }

        /// <summary>
        /// Это локальный сайт?
        /// </summary>
        public bool IsLocalHost()
        {
            return browser.Url.Contains(".lh") || Regex.IsMatch(browser.Url, @"\d+");
        }

        public void SaveOrderId()
        {
            if(IsOrderCompletePage) File.WriteAllText("Orderid.txt", Regex.Match(browser.Url, @"\d+$").Value);
        }
        #endregion

        #region "Methods for shop pages"
        

        public const string StrinFromLink1000Score = "1000";
        public const string StrinFromAuthUser = "ЛИЧНЫЙ";
        public const string NotFoundProductItemsMessage = "Сожалеем, но по вашему запросу ничего не найдено. Перейти в каталог.";
        //public const string notFoundProductItemsMessageForAnon = "МЫ ОЧЕНЬ СОЖАЛЕЕМ, НО ТОВАРЫ, КОТОРЫЕ ВЫ ИСКАЛИ, УЖЕ КУПИЛИ. МЫ ПОСТАРАЛИСЬ ПОДОБРАТЬ ДЛЯ ВАС ПОХОЖИЕ ВЕЩИ, А ЧТОБЫ ШОПИНГ СТАЛ ЕЩЕ ПРИЯТНЕЕ, ДАРИМ 1000 РУБЛЕЙ НА ПЕРВУЮ ПОКУПКУ!";

        [FindsBy(How = How.ClassName, Using = "quick_search")]
        [CacheLookup]
        public IWebElement QuickSearch;

        // email инпут при нулевом поиске под анонимом
        [FindsBy(How = How.ClassName, Using = "mail")]
        [CacheLookup]
        public IWebElement Mail;

        // кнопка "отправить" при нулевом поиске под анонимом
        [FindsBy(How = How.ClassName, Using = "sub")]
        [CacheLookup]
        public IWebElement Submit;

        [FindsBy(How = How.Id, Using = "emptyregister")]
        [CacheLookup]
        public IWebElement Emptyregister;

        // будет использоваться для проверки "зашел/не зашел"
        [FindsBy(How = How.ClassName, Using = "personal")]
        [CacheLookup]
        public IWebElement PersonalBlockLink;

        /*[FindsBy(How = How.Id, Using = "1000score")]
        public IWebElement linkOf1000ScorePopup;*/

        // тестовый вариант, чтоб не падал тест CartTests.CheckFirstRegistrationDiscountOnMainPage
        public IWebElement LinkOf1000ScorePopup
        {
            get { return FindElement(browser, By.Id("1000score"), 15); }
        }

        [FindsBy(How = How.ClassName, Using = "errors")]
        [CacheLookup]
        public IWebElement ErrorMessage;

        [FindsBy(How = How.Name, Using = "USER_EMAIL")]
        [CacheLookup]
        public IWebElement PopupInput;

        [FindsBy(How = How.Name, Using = "female_submit")]
        [CacheLookup]
        public IWebElement PopupSubmit;

        [FindsBy(How = How.ClassName, Using = "forgotpass")]
        [CacheLookup]
        public IWebElement ForgotPass;

        [FindsBy(How = How.Id, Using = "price-range-nums")]
        [CacheLookup]
        public IWebElement PriceRangeNums;

        [FindsBy(How = How.Id, Using = "authemailinput")]
        [CacheLookup]
        public IWebElement AuthEmailInput;

        [FindsBy(How = How.Id, Using = "authsendemail")]
        [CacheLookup]
        public IWebElement AuthSendEmail;
        
        /// <summary>
        /// Показывает количество товаров в Корзине
        /// </summary>
        /// <returns>Количество товаров</returns>
        public int CountOfProductInCart()
        {
            try
            {
                return
                    Convert.ToInt32(Regex.Match(FindElement(browser, By.ClassName("cart")).FindElement(By.TagName("a")).Text, @"\d+").Value);
            }
            catch (FormatException)
            {
                return 0;
            }
        }

        /// <summary>
        /// Метод удаления товара из корзины
        /// </summary>
        /// <param name="productItem">Порядковый номер удаляемого товара в Корзине</param>
        public void DeleteProductFromCart(int productItem = 1)
        {
            if (IsElementPresent(By.ClassName("cart")) && CountOfProductInCart() > 0)
            {
                JsAction("$('.quick_cart_product_one:eq(" + (productItem - 1) + ") a').click()");
            }
        }

        /// <summary>
        /// Метод поиска товара на сайте
        /// </summary>
        /// <param name="findValue">Строка запроса</param>
        public void FindItems(string findValue)
        {
            JsAction("$('.quick_search:eq(0) input:eq(0)').val('" + findValue + "')");
            JsAction("$('.quick_search:eq(0) input:eq(1)').click()");
        }

        /// <summary>
        /// Метод нажятия на "ОК" при поиске товаров
        /// </summary>
        public void ClickToOKOnFindPage()
        {
            JsAction("$('.sub').click()");
        }

        /// <summary>
        /// Метод подсчета количеста товаров на странице каталога, бренда, etc
        /// </summary>
        /// <returns>Количество товаров</returns>
        public int GetCountProductItemsOnPage()
        {
            return FindElements(browser, By.ClassName("product_one_parent"), 20).Count;
        }

        /// <summary>
        /// Каталог пустой?
        /// </summary>
        public bool IsCatalogEmpty()
        {
            return JsActionBool("$('.product_one_parent').size() == 0");
        }

        /// <summary>
        /// Метод регистрации пользователя через PopUp окно
        /// </summary>
        /// <param name="type">Тип регистрации</param>
        /// <returns>Возвращает объект страницы (вернет дочерний экземпляр объекта)</returns>
        public Page RegisterThroughMainPopUp(string type = null)
        {
            switch (type)
            {
                case "incorrect":
                    PopupInput.SendKeys("sdfsdf@sdfsdf");
                    break;
                case "alreadyExistsEmail":
                    PopupInput.SendKeys("dgordin@brand-in-trend.ru");
                    break;
                case "empty":
                    PopupInput.SendKeys("");
                    break;
                case "correct":
                    PopupInput.SendKeys(ServiceAction.RandomEmail);
                    break;
                default:
                    PopupInput.SendKeys(ServiceAction.RandomEmail);
                    break;
            }
            PopupSubmit.Click();
            return new Page(browser);
        }
        #endregion
    }
}