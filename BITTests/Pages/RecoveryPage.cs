﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using BITTests.Pages.Elements;

namespace BITTests.Pages
{
    public class RecoveryPage : Page
    {
        public HeaderBlock HeaderElement;

        public static readonly string Url = ServiceAction.Host + "/content/help/password_recovery/";

        public RecoveryPage(IWebDriver browser)
            : base(browser)
        {
            if (browser.Url != Url)
            {
                browser.Navigate().GoToUrl(Url);
            }
            HeaderElement = new HeaderBlock(browser);
            SleepOn();
            ClosePopupWindow();
        }

        [FindsBy(How = How.Name, Using = "USER_EMAIL")]
        [CacheLookup]
        public IWebElement EmailInput;

        [FindsBy(How = How.Name, Using = "send_account_info")]
        [CacheLookup]
        public IWebElement EmailSendButton;

        [FindsBy(How = How.ClassName, Using = "flw100")]
        [CacheLookup]
        public IWebElement Flw100;

        public string GetRecoverySuccessText()
        {
            return FindElement(browser, By.ClassName("flw100"), 20).FindElement(By.TagName("p")).Text;
        }

        public void SendRecoveryQuery(string email)
        {
            EmailInput.SendKeys(email);
            EmailSendButton.Click();
        }
    }
}
