﻿using BITTests.Pages.Elements;
using OpenQA.Selenium;

namespace BITTests.Pages
{
    public class MainPage : Page
    {
        public HeaderBlock HeaderElement;

        public MainPage(IWebDriver browser) : base(browser)
        {
            browser.Navigate().GoToUrl(ServiceAction.Host);
            HeaderElement = new HeaderBlock(browser);
            SleepOn();
            ClosePopupWindow();
        }

        public MainPage(IWebDriver browser, bool popupClose) : base(browser, popupClose)
        {
            browser.Navigate().GoToUrl(ServiceAction.Host);
            HeaderElement = new HeaderBlock(browser);
            SleepOn();
            if (popupClose)
                ClosePopupWindow();
        }    
    }
}
