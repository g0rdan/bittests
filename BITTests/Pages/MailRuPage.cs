﻿using System.Linq;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Support.PageObjects;

namespace BITTests.Pages
{
    class MailRuMainPage : CommonLayer
    {
        public MailRuMainPage(IWebDriver browser)
            : base(browser)
        {
            browser.Navigate().GoToUrl("http://mail.ru");
        }

        [FindsBy(How = How.Id, Using = "mailbox__login")]
        [CacheLookup]
        public IWebElement LoginInput;

        [FindsBy(How = How.Id, Using = "mailbox__login__domain")]
        [CacheLookup]
        public IWebElement DomainSelect;

        [FindsBy(How = How.Id, Using = "mailbox__password")]
        [CacheLookup]
        public IWebElement PasswordInput;

        [FindsBy(How = How.Id, Using = "mailbox__auth__button")]
        [CacheLookup]
        public IWebElement LoginButton;

        public MailRuMessagePage SignIn()
        {
            LoginInput.SendKeys("gor.dan");
            new SelectElement(FindElement(browser, By.Id("mailbox__login__domain"))).SelectByValue("bk.ru");
            PasswordInput.SendKeys("m77753360alabama");
            LoginButton.Click();
            return new MailRuMessagePage(browser);
        }
    }

    class MailRuMessagePage : CommonLayer
    {
        public MailRuMessagePage(IWebDriver browser) : base(browser)
        {
        }

        [FindsBy(How = How.Id, Using = "ML0")]
        //[CacheLookup]
        public IWebElement MessagesBox;

        public IWebElement GetMessageElement(int messageIndex)
        {
            return FindElements(browser, By.ClassName("messageline"), 20)[messageIndex - 1];
        }

        public string GetNameFromMessage(int messageIndex = 0)
        {
            if (messageIndex == 0)
            {
                return GetFirstMessageElement().FindElement(By.ClassName("messageline__body__name")).Text;
            }
            return GetMessageElement(messageIndex).FindElement(By.ClassName("messageline__body__name")).Text;
        }

        public IWebElement GetFirstMessageElement()
        {
            return FindElements(browser, By.ClassName("messageline"), 20).First();
        }
    }
}
