﻿using System.Diagnostics;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using BITTests.Pages.Elements;

namespace BITTests.Pages
{
    public class BrandPage : Page
    {
        public HeaderBlock HeaderElement;
        public FilterBlock FilterBlock;
        public SortBlock SortBlock;

        [FindsBy(How = How.ClassName, Using = "right_sd")]
        [CacheLookup]
        public IWebElement Description;

        public BrandPage(IWebDriver browser, string url) : base(browser, url)
        {
            browser.Navigate().GoToUrl(ServiceAction.Host + url);
            HeaderElement = new HeaderBlock(browser);
            FilterBlock = new FilterBlock(browser);
            SortBlock = new SortBlock(browser);
            SleepOn();
        }

        public BrandPage(IWebDriver browser, bool popupClose = true) : base(browser, popupClose)
        {
            browser.Navigate().GoToUrl(ServiceAction.GetRandomBrandPage() + "/women");
            if (FindElements(browser, By.ClassName("product_one_parent")).Count == 0)
            {
                browser.Navigate().GoToUrl(browser.Url.Replace("/women", "/men"));
            }
            HeaderElement = new HeaderBlock(browser);
            FilterBlock = new FilterBlock(browser);
            SortBlock = new SortBlock(browser);
            SleepOn();
            if (popupClose)
                ClosePopupWindow();
        }

        public BrandPage(IWebDriver browser, string url, bool popupClose) : base(browser, url, popupClose)
        {
            browser.Navigate().GoToUrl(ServiceAction.Host + url);
            HeaderElement = new HeaderBlock(browser);
            FilterBlock = new FilterBlock(browser);
            SortBlock = new SortBlock(browser);
            SleepOn();
            if (popupClose)
                ClosePopupWindow();
        }

        public BrandPage ClickGenderLink()
        {
            try
            {
                Description.FindElement(By.TagName("a")).Click();
                return new BrandPage(browser, browser.Url.Replace(ServiceAction.Host, ""));
            }
            catch (NoSuchElementException)
            {
                //Debug.WriteLine("Гендерной ссылки нет на этой странице");
            }
            return null;
        }
    }
}
