﻿using System;
using System.Text.RegularExpressions;
using BITTests.Pages.Elements;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BITTests.Pages
{
    public class ProductPage : Page
    {
        public HeaderBlock HeaderElement;

        public ProductPage(IWebDriver browser, string url)
            : base(browser, url)
        {
            browser.Navigate().GoToUrl(url);
            HeaderElement = new HeaderBlock(browser);
            SleepOn();
            ClosePopupWindow();
        }

        public ProductPage(IWebDriver browser, bool popupClose)
            : base(browser, popupClose)
        {
            browser.Navigate().GoToUrl(ServiceAction.GetRandomProductPage());
            HeaderElement = new HeaderBlock(browser);
            SleepOn();
            if (popupClose)
                ClosePopupWindow();
        }

        public ProductPage(IWebDriver browser, string url, bool popupClose)
            : base(browser, url, popupClose)
        {
            browser.Navigate().GoToUrl(url);
            HeaderElement = new HeaderBlock(browser);
            SleepOn();
            if (popupClose)
                ClosePopupWindow();
        }

        [FindsBy(How = How.ClassName, Using = "add2cartbutton")]
        [CacheLookup]
        public IWebElement Buybutton;

        [FindsBy(How = How.ClassName, Using = "oneclick2orderbutton")]
        [CacheLookup]
        public IWebElement Oneclick2Orderbutton;

        [FindsBy(How = How.ClassName, Using = "cartlink")]
        [CacheLookup]
        public IWebElement Cartlink;

        [FindsBy(How = How.Id, Using = "heart")]
        [CacheLookup]
        public IWebElement Heart;

        [FindsBy(How = How.Id, Using = "qtb_name")]
        [CacheLookup]
        public IWebElement FirstNameForOneclickForm;

        [FindsBy(How = How.Id, Using = "qtb_lastname")]
        [CacheLookup]
        public IWebElement LastNameForOneclickForm;

        [FindsBy(How = How.Id, Using = "qtb_phone")]
        [CacheLookup]
        public IWebElement PhoneNumberForOneclickForm;

        [FindsBy(How = How.Id, Using = "qtb_city")]
        [CacheLookup]
        public IWebElement CityForOneclickForm;

        [FindsBy(How = How.Id, Using = "qtb_mail")]
        [CacheLookup]
        public IWebElement EmailForOneclickForm;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"prodformblock\"]/div[3]/div[5]/div[13]/div/input")]
        [CacheLookup]
        public IWebElement SubmitButtonForOneclickForm;

        public void CreateFastOrder()
        {
            Oneclick2Orderbutton.Click();
            FirstNameForOneclickForm.SendKeys("test");
            LastNameForOneclickForm.SendKeys("test");
            PhoneNumberForOneclickForm.SendKeys("89500302093");
            CityForOneclickForm.SendKeys("Владивосток");
            EmailForOneclickForm.SendKeys(ServiceAction.RandomEmail);
            SubmitButtonForOneclickForm.Click();
            WaitForJQuery();
        }

        public void BuyButtonClick()
        {
            //ClosePopupWindow(PlatformMethod.Selenium);
            JsAction("$('.add2cartbutton a').first().click()");
            //SleepOn(10);
            WaitForJQuery();
        }

        public int GetPrice()
        {
            Regex regex = new Regex(@"\D+");
            return Convert.ToInt32(regex.Replace(FindElement(browser, By.ClassName("newprice")).Text, ""));
        }
    }
}