﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BITTests.Pages
{
    public class PromoPage : Page
    {
        public PromoPage(IWebDriver browser, string urn)
            : base(browser)
        {
            url = ServiceAction.Host + "/promo/" + urn;
            browser.Navigate().GoToUrl(url);
            SleepOn();
            ClosePopupWindow();
        }

        [FindsBy(How = How.Name, Using = "USER_EMAIL")]
        [CacheLookup]
        public IWebElement EmailInput;

        [FindsBy(How = How.Name, Using = "female_submit")]
        [CacheLookup]
        public IWebElement FemaleSubmit;

        [FindsBy(How = How.Name, Using = "male_submit")]
        [CacheLookup]
        public IWebElement MaleSubmit;

        public MainPage RegisterFromPromoPage(string regType, string genderType)
        {
            switch (regType)
            {
                case "incorrect":
                    EmailInput.SendKeys("sdfsdf@sdfsdf");
                    break;
                case "alreadyExistsEmail":
                    EmailInput.SendKeys("dgordin@brand-in-trend.ru");
                    break;
                case "empty":
                    EmailInput.SendKeys("");
                    break;
                case "correct":
                    EmailInput.SendKeys(ServiceAction.RandomEmail);
                    break;
                default:
                    EmailInput.SendKeys(ServiceAction.RandomEmail);
                    break;
            }
            switch (genderType)
            {
                case "female":
                    FemaleSubmit.Click();
                    break;
                case "male":
                    MaleSubmit.Click();
                    break;
                default:
                    FemaleSubmit.Click();
                    break;
            }
            return new MainPage(browser);
        }
    }
}
