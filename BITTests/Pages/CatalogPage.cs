﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using BITTests.Pages.Elements;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BITTests.Pages
{
    public class CatalogPage : Page
    {
        public HeaderBlock HeaderBlock;
        public FilterBlock FilterBlock;
        public SortBlock SortBlock;

        public CatalogPage(IWebDriver browser, string url) : base(browser, url)
        {
            browser.Navigate().GoToUrl(ServiceAction.Host + url);
            HeaderBlock = new HeaderBlock(browser);
            FilterBlock = new FilterBlock(browser);
            SortBlock = new SortBlock(browser);
            SleepOn();
            ClosePopupWindow();
        }

        public CatalogPage(IWebDriver browser, bool popupClose = true) : base(browser, popupClose)
        {
            browser.Navigate().GoToUrl(ServiceAction.GetRandomCatalogPage());
            HeaderBlock = new HeaderBlock(browser);
            FilterBlock = new FilterBlock(browser);
            SortBlock = new SortBlock(browser);
            SleepOn();
            if (popupClose)
                ClosePopupWindow();
        }

        public CatalogPage(IWebDriver browser, string url, bool popupClose)
            : base(browser, url, popupClose)
        {
            browser.Navigate().GoToUrl(ServiceAction.Host + url);
            HeaderBlock = new HeaderBlock(browser);
            FilterBlock = new FilterBlock(browser);
            SortBlock = new SortBlock(browser);
            SleepOn();
            if (popupClose)
                ClosePopupWindow();
        }

        [FindsBy(How = How.ClassName, Using = "flw100")]
        [CacheLookup]
        public IWebElement Flw100;

        [FindsBy(How = How.ClassName, Using = "breadcrumbs")]
        [CacheLookup]
        public IWebElement Breadcrumbs;

        [FindsBy(How = How.ClassName, Using = "filtervalone")]
        [CacheLookup]
        public IWebElement Filtervalone;

        [FindsBy(How = How.Id, Using = "filteron_sale")]
        [CacheLookup]
        public IWebElement FilteronSale;

        #region FUNCTIONAL METHODS

        /// <summary>
        /// Переключает страницы в пагинаторе на странице каталога
        /// </summary>
        /// <param name="type">тип переключения - либо единичный (по страничный), либо групповой (показывает
        /// блоками количество товара)</param>
        /// <returns>возвращает массив айдишников товаров до и после</returns>
        public List<string> ChangePage(string type)
        {
            var result = new List<string>();
            if (FindElement(browser, By.ClassName("pageone")).FindElements(By.TagName("span")).Count < 2)
                browser.Navigate().GoToUrl(ServiceAction.Host + "/catalog");
            switch (type)
            {
                case "one":
                    result.AddRange(FindElements(browser, By.ClassName("product_one_parent"), 20).Select(product => product.GetAttribute("id")));
                    break;
                case "group":

                    break;
            }
            JsAction("$('div.pageone:eq(1) a').click()"); // выбираем вторую страницу пагинатора
            return result;
        }

        public IWebElement GetFirstProduct()
        {
            return FindElements(browser, By.ClassName("product_one_parent")).First();
        }

        public ProductPage GetProductWithFewSize()
        {
            string url = "";
            var products = FindElements(browser, By.ClassName("qovalscloser2"));
            foreach (IWebElement element in products)
            {
                if (element.FindElements(By.TagName("a")).Count > 1)
                {
                    url = element.FindElement(By.TagName("a")).GetAttribute("href");
                    break;
                }
            }
            return new ProductPage(browser, url);
        }

        public string GetFirstProductHref()
        {
            return FindElements(browser, By.ClassName("product_one_parent")).First().FindElement(By.ClassName("preload")).GetAttribute("href");
        }

        public void SetDiscountValueInFilter(string values)
        {
            JsAction("$('.filterselectname:last').mouseover()");
            switch (values)
            {
                case "all":
                    JsAction("$('ul.jscrolled:last > li:eq(0)').click()");
                    break;
                case "30":
                    JsAction("$('ul.jscrolled:last > li:eq(1)').click()");
                    break;
                case "30-50":
                    JsAction("$('ul.jscrolled:last > li:eq(2)').click()");
                    break;
                case "50-70":
                    JsAction("$('ul.jscrolled:last > li:eq(3)').click()");
                    break;
            }
        }

        public void SetCountViewItems(string type)
        {
            switch (type)
            {
                case "24":
                    JsAction("$('.onpagelist:eq(0) .onpageone:eq(0) a').click()");
                    break;
                case "48":
                    JsAction("$('.onpagelist:eq(0) .onpageone:eq(1) a').click()");
                    break;
                case "96":
                    JsAction("$('.onpagelist:eq(0) .onpageone:eq(2) a').click()");
                    break;
                case "all":
                    JsAction("$('.onpagelist:eq(0) .onpageone:eq(3) a').click()");
                    break;
            }
        }

        #endregion
    }
}
