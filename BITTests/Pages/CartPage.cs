﻿using System;
using System.Linq;
using System.Text.RegularExpressions;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;

namespace BITTests.Pages
{
    public class CartPage : Page
    {
        public int ResultPriceOfOrder;

        public CartPage(IWebDriver browser)
            : base(browser)
        {
            browser.Navigate().GoToUrl(ServiceAction.Host + "/cart");
            SleepOn();
            ClosePopupWindow();
        }

        [FindsBy(How = How.ClassName, Using = "to_secondstep")]
        [CacheLookup]
        public IWebElement ButtonTo2Step;

        [FindsBy(How = How.Id, Using = "secondstep")]
        [CacheLookup]
        public IWebElement Secondstep;

        [FindsBy(How = How.ClassName, Using = "goto_confirm")]
        [CacheLookup]
        public IWebElement ButtonTo4Step;

        [FindsBy(How = How.ClassName, Using = "goto_end")]
        [CacheLookup]
        public IWebElement ButtonFinishStep;

        [FindsBy(How = How.Id, Using = "authform")]
        [CacheLookup]
        public IWebElement Authform;

        [FindsBy(How = How.Id, Using = "email")]
        [CacheLookup]
        public IWebElement EmailInputLeft;

        [FindsBy(How = How.ClassName, Using = "passexist")]
        [CacheLookup]
        public IWebElement PassInputLeft;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"name_block\"]/div[1]/input[1]")]
        [CacheLookup]
        public IWebElement FirstNameInputLeft;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"name_block\"]/div[1]/input[2]")]
        [CacheLookup]
        public IWebElement LastNameInputLeft;

        [FindsBy(How = How.Id, Using = "phone")]
        [CacheLookup]
        public IWebElement PhoneNumberInputLeft;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"city_block\"]/div[1]/div[1]/div/select[1]")]
        [CacheLookup]
        public IWebElement CitySelectLeft;

        [FindsBy(How = How.Id, Using = "p17")]
        [CacheLookup]
        public IWebElement PayOnlineRadiobutton;

        [FindsBy(How = How.Id, Using = "d15")]
        [CacheLookup]
        public IWebElement DHLRadiobutton;

        [FindsBy(How = How.ClassName, Using = "sum")]
        [CacheLookup]
        public IWebElement SummPrice;

        [FindsBy(How = How.XPath, Using = "//*[@id=\"firststep\"]/div[1]/div[1]/div[5]/span[2]")]
        //[CacheLookup]
        public IWebElement PriceOfFirstProductInCart;

        [FindsBy(How = How.Id, Using = "fast_msk")]
        [CacheLookup]
        public IWebElement FastSelectMsk;

        [FindsBy(How = How.ClassName, Using = "doyouhave")]
        [CacheLookup]
        public IWebElement inputPromoCode;

        [FindsBy(How = How.ClassName, Using = "promo")]
        [CacheLookup]
        public IWebElement PromoBlock;

        [FindsBy(How = How.Id, Using = "coupon")]
        [CacheLookup]
        public IWebElement Coupon;

        [FindsBy(How = How.ClassName, Using = "out")]
        [CacheLookup]
        public IWebElement DiscountPercentParent;

        [FindsBy(How = How.Id, Using = "cnt_items_delay")]
        [CacheLookup]
        public IWebElement CountItemsDelay;

        [FindsBy(How = How.Id, Using = "cnt_basket")]
        [CacheLookup]
        public IWebElement CountBasket;

        public IWebElement TotalPrice
        {
            get { return FindElements(browser, By.ClassName("total_price")).First(); }
        }

        public int GetCountOfProduct(int itemValue)
        {
            return Convert.ToInt32(JsActionString("$('.numb:eq(" + (itemValue - 1) + ")').val()"));
        }

        public void ChangeCountOfProduct(int itemValue, string changeType)
        {
            JsAction("$('.one_card:eq(" + (itemValue - 1) + ") ." + changeType + " a').click()");
        }

        public void AddOtherSize(int itemValue, int sizeValue)
        {
            JsAction("$('.one_card:eq(" + (itemValue - 1) + ") .addsizes-values a:eq(" + (sizeValue - 1) + ")').click()");
        }

        public bool IsDevireryExist(string deliveryName)
        {
            switch (deliveryName)
            {
                case "dhl":
                    return JsActionBool("$('#d15').size() != 0");
                case "courier":
                    return JsActionBool("$('#d6').size() != 0");
                case "pickpoint":
                    return JsActionBool("$('#d12').size() != 0");
                case "ems":
                    return JsActionBool("$('#d10').size() != 0");
                case "spsr":
                    return JsActionBool("$('#d11').size() != 0");
                default:
                    return false;
            }
        }

        public void AuthInCart(string email, string password, string authPlace)
        {
            switch (authPlace)
            {
                case "leftBlock":
                    FindElement(browser, By.Id("email")).SendKeys(email);
                    FindElement(browser, By.ClassName("passexist")).SendKeys(password);
                    JsAction("$('.correct .ok').click()");
                    WaitForJQuery();
                    break;
                case "rightBlock":
                    Authform.FindElement(By.ClassName("email"))
                            .FindElement(By.TagName("input"))
                            .SendKeys(email);
                    Authform.FindElement(By.ClassName("pass"))
                            .FindElement(By.TagName("input"))
                            .SendKeys(password);
                    Authform.FindElement(By.ClassName("remember")).Click();
                    break;
            }
            WaitForJQuery();
        }

        public void Registration()
        {
            EmailInputLeft.SendKeys(ServiceAction.RandomEmail);
            FirstNameInputLeft.SendKeys("test");
            LastNameInputLeft.SendKeys("test");
            PhoneNumberInputLeft.SendKeys("89500302093");
            FastSelectMsk.Click();
            WaitForJQuery();
        }

        public void Registration(string email)
        {
            EmailInputLeft.SendKeys(email);
            FirstNameInputLeft.SendKeys("test");
            LastNameInputLeft.SendKeys("test");
            PhoneNumberInputLeft.SendKeys("89500302093");
            FastSelectMsk.Click();
            WaitForJQuery();
        }

        public void Registration(string email, string phone)
        {
            EmailInputLeft.SendKeys(email);
            FirstNameInputLeft.SendKeys("test");
            LastNameInputLeft.SendKeys("test");
            PhoneNumberInputLeft.SendKeys(phone);
            FastSelectMsk.Click();
            WaitForJQuery();
        }

        public int GetCurrentPrice(string type = "sum")
        {
            int result;
            switch (type)
            {
                case "sum":
                    result = Convert.ToInt32(Regex.Match(SummPrice.Text.Replace(" ", ""), @"\d+").Value);
                    break;
                case "totalPrice":
                    result = Convert.ToInt32(Regex.Match(FindElement(browser, By.ClassName("total_price"), 12).Text.Replace(" ", ""), @"\d+").Value);
                    break;
                default:
                    result = Convert.ToInt32(Regex.Match(SummPrice.Text.Replace(" ", ""), @"\d+").Value);
                    break;
            }
            return result;
        }

        public void ClickTo2Button()
        {
            FindElement(browser, By.ClassName("to_secondstep")).FindElement(By.TagName("a")).Click();
        }

        public void ClickTo3Button()
        {
            JsAction("$('.go_to_delivery a').click()");
        }

        public void ClickTo4Button()
        {
            JsAction("$('.goto_confirm a').click()");
        }

        public void ClickToFinishButton()
        {
            ResultPriceOfOrder = Convert.ToInt32(Regex.Match(TotalPrice.Text.Replace(" ", "").Replace(".–", ""), @"\d+").Value);
            JsAction("$('.goto_end a').click()");
        }

        public void ClickPayonlineButton()
        {
            PayOnlineRadiobutton.Click();
            WaitForJQuery();
        }

        public void ClickDhlButton()
        {
            FindElement(browser, By.Id("d15")).Click();
        }

        public void InputPromoCode(string promoCode)
        {
            JsAction("$('#coupon input:first').val('" + promoCode + "')");
            JsAction("$('#coupon input:last').click()");
            JsAction("$('.closecupon').click()");
        }

        public int GetFirstDiscoutPercent()
        {
            return Convert.ToInt32(DiscountPercentParent.FindElement(By.TagName("span")).Text);
        }

        public int GetLastDiscoutPercent()
        {
            return Convert.ToInt32(FindElements(browser, By.ClassName("out")).Last().FindElement(By.TagName("span")).Text);
        }

        public void ClickToX(int item)
        {
            int countProducts = GetProductsCount("cart");
            if (countProducts >= item)
            {
                JsAction("$('.remove_item:eq(" + (item - 1) + ")').click()");
            }
        }

        public void ClickToDelay(int item)
        {
            int countProducts = GetProductsCount("cart");
            if (countProducts >= item)
            {
                JsAction("$('.delay:eq(" + (item - 1) + ")').click();");
            }
        }

        public void ClickToKill(int item)
        {
            int countProducts = GetProductsCount("cart");
            if (countProducts >= item)
            {
                JsAction("$('.kill:eq(" + item + ")').click()");
            }
        }

        public void AddDelayProductInOrder(int item)
        {
            int countProducts = GetProductsCount("delay");
            if (countProducts >= item)
            {
                JsAction("$('.addtoorder:eq(" + (item - 1) + ") a').click()");
            }
        }

        public void OpenDelayWindow()
        {
            JsAction("$('#cnt_items_delay a').click()");
        }

        public void CloseDelayWindow()
        {
            JsAction("$('#cart_favorites .a-form-close-button').click()");
        }

        public int GetProductsCount(string type)
        {
            int result = 0;
            switch (type)
            {
                case "cart":
                    result = FindElements(browser, By.ClassName("one_card"), 10).Count;
                    break;
                case "delay":
                    result = FindElements(browser, By.ClassName("one_favorite"), 10).Count;
                    break;
            }
            return result;
        }

        public int GetDelayProductsCountInFirstPlace()
        {
            return Convert.ToInt32(Regex.Match(FindElement(browser, By.Id("cnt_items_delay")).FindElement(By.TagName("span")).Text, @"\d+").Value);
        }

        public int GetDelayProductsCountInSecondPlace()
        {
            return Convert.ToInt32(Regex.Match(CountBasket.FindElement(By.TagName("span")).Text, @"\d+").Value);
        }

        public void ClearCart()
        {
            JsAction("$('.remove_item').click()");
            JsAction("$('.kill').click()");
        }

        public int GetPrice()
        {
            return Convert.ToInt32(Regex.Match(TotalPrice.Text.Replace(" ", "").Replace(".–", ""), @"\d+").Value);
        }
    }
}
