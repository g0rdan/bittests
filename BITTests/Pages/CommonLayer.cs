﻿using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Drawing.Imaging;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;
using OpenQA.Selenium.Support.UI;

namespace BITTests.Pages
{
     public abstract class CommonLayer
    {
        protected IWebDriver browser;
        protected IJavaScriptExecutor jsExec;
        protected string url;

        public enum PlatformMethod
        {
            JS,
            Selenium
        }

        public enum SortType
        {
            ASC,
            DESC
        }

        public enum FilterType
        {
            Brand,
            Size,
            Color,
            Style,
            Material,
            Season,
            Collection,
            Sale
        }

        protected CommonLayer(IWebDriver browser)
        {
            this.browser = browser;
            PageFactory.InitElements(browser, this);
            jsExec = browser as IJavaScriptExecutor;
        }
        protected void SleepOn(int seconds = 3)
        {
            Thread.Sleep(seconds * 1000);
            WaitForJQuery();
        }

        public string GetHostUrl()
        {
            return browser.Url;
        }

        public void Refresh()
        {
            browser.Navigate().Refresh();
        }

        public void JsAction(string script)
        {
            WaitForJQuery();
            jsExec.ExecuteScript("$(document).ready(function(){" + script + "})");
            WaitForJQuery();
        }

        public string JsActionString(string script)
        {
            return (string)jsExec.ExecuteScript("return " + script);
        }

        public int JsActionInt(string script)
        {
            return Convert.ToInt32(jsExec.ExecuteScript("return " + script));
        }

        public bool JsActionBool(string script)
        {
            return (bool)jsExec.ExecuteScript("return " + script);
        }

        public void WaitForJQuery()
        {
            try
            {
                while (true)
                {
                    if (JsActionBool("jQuery.active == 0"))
                        break;
                    Thread.Sleep(100);
                }
            }
            catch (InvalidOperationException e)
            {
                Debug.WriteLine(e.Message);
                browser.Navigate().GoToUrl(ServiceAction.Host);
            }
        }

        public bool WaitForElements(IWebDriver browser, By by, int timeoutInSeconds = 10)
        {
            var wait = new WebDriverWait(browser, TimeSpan.FromSeconds(timeoutInSeconds));
            return wait.Until(drv => drv.FindElement(by)).Displayed;
        }

        public IWebElement FindElement(IWebDriver browser, By by, int timeoutInSeconds = 10)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(browser, TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(drv => drv.FindElement(by));
            }
            return browser.FindElement(by);
        }

        public ReadOnlyCollection<IWebElement> FindElements(IWebDriver browser, By by, int timeoutInSeconds = 10)
        {
            if (timeoutInSeconds > 0)
            {
                var wait = new WebDriverWait(browser, TimeSpan.FromSeconds(timeoutInSeconds));
                return wait.Until(drv => drv.FindElements(by));
            }
            return browser.FindElements(by);
        }

        public void TakeScreenshot(string fileName)
        {
            Screenshot ss = ((ITakesScreenshot)browser).GetScreenshot();
            ss.SaveAsFile(ServiceAction.PathToScreenshots + fileName, ImageFormat.Jpeg);
        }

        public bool IsElementPresent(By by)
        {
            try
            {
                FindElement(browser, by, 15);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
