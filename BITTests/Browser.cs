﻿using System.Xml;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.IE;

namespace BITTests
{
    public static class Browser
    {
        private static readonly IWebDriver Instance = null;

        public static IWebDriver GetInstance()
        {
            using (XmlReader reader = XmlReader.Create("../../Config/Driver.xml"))
            {
                reader.ReadToFollowing("driverConfiguration");
                reader.MoveToAttribute("targetDriver");
                switch (reader.Value)
                {
                    case "Chrome":
                        return Instance ?? new ChromeDriver();
                    case "IE":
                        return Instance ?? new InternetExplorerDriver();
                    case "Firefox":
                        return Instance ?? new FirefoxDriver();
                    default:
                        return Instance;
                }
            }
        }
    }
}
