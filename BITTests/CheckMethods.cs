﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using BITTests.Pages;
using OpenQA.Selenium;

namespace BITTests
{
    public abstract partial class Test
    {
        public bool IsProductsHasSortPriceBy(CommonLayer.SortType sortType)
        {
            var currPrices = Wait.Until(drv => drv.FindElements(By.ClassName("curprice")));
            if (currPrices.Count >= 3)
            {
                for (int i = 0; i < currPrices.Count - 1; i++)
                {
                    int firstPrice = Convert.ToInt32(Regex.Match(currPrices[i].Text.Replace(" ", ""), @"\d+").Value);
                    int secondPrice = Convert.ToInt32(Regex.Match(currPrices[i + 1].Text.Replace(" ", ""), @"\d+").Value);
                    switch (sortType)
                    {
                        case CommonLayer.SortType.ASC:
                            if (firstPrice > secondPrice)
                                return false;
                            break;
                        case CommonLayer.SortType.DESC:
                            if (firstPrice < secondPrice)
                                return false;
                            break;
                    }
                    if (i == (currPrices.Count - 2)) break;
                }
            }
            else
            {
                Debug.WriteLine("Продукта меньше 3");
            }
            return true;
        }

        public bool IsProductsHasSortByPopular()
        {
            var productsWebElements = Wait.Until(drv => drv.FindElements(By.ClassName("product_one")));
            if (productsWebElements == null || productsWebElements.Count == 0)
            {
                return false;
            }
            if (productsWebElements.Count > 6)
            {
                for (int i = 0; i < productsWebElements.Count - 2; i++)
                {
                    double firstValue, secondValue;
                    double.TryParse(Regex.Match(productsWebElements[i].FindElements(By.TagName("span"))[0].Text, @"^\d+\.\d+").Value, out firstValue);
                    double.TryParse(Regex.Match(productsWebElements[i + 1].FindElements(By.TagName("span"))[0].Text, @"^\d+\.\d+").Value, out secondValue);
                    if (firstValue < secondValue) return false;
                }
            }
            return true;
        }

        public bool IsProductsHasFilterBy(CommonLayer.FilterType filterType)
        {
            switch (filterType)
            {
                case CommonLayer.FilterType.Brand:
                    string brandName = Wait.Until(drv => drv.FindElement(By.ClassName("filtervalone"))).Text.Replace("Бренд: ", "").Replace("Х", "").Trim().ToUpper();
                    var brandElements = Wait.Until(drv => drv.FindElements(By.ClassName("brand")));
                    if (brandElements.Select(x => x.Text.Trim().ToUpper()).Count(x => x != brandName) == 0)
                        return true;
                    break;
            }
            return false;
        }

        public bool IsPageHasSameProductsFromLastPage(List<string> productsIds)
        {
            var resultAfter = Wait.Until(drv => drv.FindElements(By.ClassName("product_one_parent")));
            if (resultAfter.Any(after => productsIds.Any(before => after != null && after.GetAttribute("id").Equals(before))))
                return false;
            return true;
        }
    }
}
