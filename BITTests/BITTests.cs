﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Xml;
using BITTests.Pages;
using BITTests.Pages.Elements;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using OpenQA.Selenium.Support.UI;
using Services;


namespace BITTests
{
    public abstract partial class Test
    {
        public IWebDriver browser = Browser.GetInstance();
        public WebDriverWait Wait;
        public tORM ORM;

        protected MainPage _mainPage;
        protected ProductPage _productPage;
        protected CatalogPage _catalogPage;
        protected CartPage _cartPage;
        protected BrandPage _brandPage;
        protected PromoPage _promoPage;

        protected Regex _regex;

        [TestFixtureSetUp]
        public void TestFictureSetup()
        {
            ORM = new tORM(tORM.DBForConnection.Anton);
            Wait = new WebDriverWait(browser, TimeSpan.FromSeconds(20));
        }

        [TestFixtureTearDown]
        public void Dispose()
        {
            ORM.Close();
            browser.Quit();
        }

        [SetUp]
        public void Setup()
        {
            /*browser = new RemoteWebDriver(new Uri("http://localhost:4444/wd/hub"), DesiredCapabilities.Firefox());*/
            /*using (XmlReader reader = XmlReader.Create("../../Config/Driver.xml"))
            {
                reader.ReadToFollowing("driverConfiguration");
                reader.MoveToAttribute("width");
                int width = Convert.ToInt32(reader.Value);
                reader.MoveToAttribute("height");
                int height = Convert.ToInt32(reader.Value);
                //browser.Manage().Window.Size = new Size(width, height);
            }*/
            
        }

        [TearDown]
        public void Teardown()
        {
            browser.Manage().Cookies.DeleteAllCookies();
        }

        public bool IsProductsHasSamePrice()
        {
            var currPrices = browser.FindElements(By.ClassName("curprice"));
            if (currPrices.Count == 1)
                return true;
            for (int i = 0; i < currPrices.Count - 2; i++)
            {
                if (!currPrices[i].Text.Equals(currPrices[i + 1].Text))
                    return false;
            }
            return true;
        }    
    }

    [TestFixture]
    class BrandTests : Test
    {
        [Test, Description("Тест проверки фильтрации цены на странице бренда (там где указывается диапазон цен)")]
        public void CheckPriceFilter()
        {
            //throw new Exception("sdfsdfs");

            _brandPage = new BrandPage(browser);
            _brandPage.FilterBlock.FilterByPrice();
            Assert.True(IsProductsHasSamePrice());
        }

        [Test, Description("Тест проверки сортировки цены по возрастанию на странице бренда")]
        public void CheckSortByPriceAsc()
        {
            _brandPage = new BrandPage(browser);
            _brandPage.SortBlock.SortByPrice(CommonLayer.SortType.ASC);
            Thread.Sleep(10000);
            Assert.True(IsProductsHasSortPriceBy(CommonLayer.SortType.ASC));
        }

        [Test, Description("Тест проверки сортировки цены по убыванию на странице бренда")]
        public void CheckSortByPriceDesc()
        {
            _brandPage = new BrandPage(browser);
            _brandPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC);
            Assert.True(IsProductsHasSortPriceBy(CommonLayer.SortType.DESC));
        }

        [Test, Description("Тест проверки сортировки по популярности (убывание) на странице бренда")]
        public void CheckSortByPopular()
        {
            _brandPage = new BrandPage(browser);
            _brandPage.HeaderElement.SignIn("dgordin@brand-in-trend.ru", "77753360");
            _brandPage.CloseSuccessAuthWindow();
            _brandPage.SortBlock.SortByPopular();
            Assert.True(IsProductsHasSortByPopular());
        }
    }

    [TestFixture]
    class RegAuthTests : Test
    {
        [Test, Description("'Тестовый тест'")]
        public void Test()
        {
            
        }

        [Test, Description("Позитивные тесты авторизации на главной")]
        [TestCase("gordin.dan@gmail.com", "123456")]
        [TestCase("dgordin@brand-in-trend.ru", "77753360")]
        public void CorrectMainAuth(string email, string pass)
        {
            _mainPage = new MainPage(browser);
            _mainPage.HeaderElement.SignIn(email, pass);
            _mainPage = new MainPage(browser); //костыль @todo: рекафторить
            StringAssert.Contains(Page.StrinFromAuthUser, _mainPage.PersonalBlockLink.Text);
        }

        [Test, Description("Негативные тесты авторизации на главной")]
        [TestCase("gordin.dan@gmail", "123456")]
        [TestCase("gordin.dan@gmail.com", "1234")]
        [TestCase("gordin.dan@gmail", "1234")]
        [TestCase("", "")]
        public void InсorrectMainAuth(string email, string pass)
        {
            _mainPage = new MainPage(browser);
            _mainPage.HeaderElement.SignIn(email, pass);
            _mainPage = new MainPage(browser); //костыль @todo: рекафторить
            StringAssert.DoesNotContain(Page.StrinFromAuthUser, _mainPage.PersonalBlockLink.Text);
        }

        [Test, Description("Позитивные тесты регистрации на главной")]
        [TestCaseSource("DivideCasesForCorrectReg")]
        public void CorrectMainReg(string email, string pass)
        {
            _mainPage = new MainPage(browser);
            _mainPage.HeaderElement.SignUp(email, pass);
            _mainPage = new MainPage(browser); //костыль @todo: рекафторить
            StringAssert.Contains(Page.StrinFromAuthUser, _mainPage.PersonalBlockLink.Text);
            //StringAssert.Contains(Page.StrinFromLink1000Score, _mainPage.LinkOf1000ScorePopup.Text);
        }

        private static readonly object[] DivideCasesForCorrectReg =
            {
                new object[] {ServiceAction.RandomEmail, ServiceAction.RandomPassword},
                new object[] {ServiceAction.RandomEmail, ""}
            };

        [Test, Description("Негативные тесты регистрации на главной")]
        [TestCaseSource("DivideCasesForIncorrectReg")]
        public void IncorrectMainReg(string email, string pass)
        {
            _mainPage = new MainPage(browser);
            _mainPage.HeaderElement.SignUp(email, pass);
            StringAssert.DoesNotContain(Page.StrinFromAuthUser, _mainPage.PersonalBlockLink.Text);
        }

        private static readonly object[] DivideCasesForIncorrectReg =
            {
                new object[] {"gordin.dan", "123456"},
                new object[] {"gordin.dan@", "123456"},
                new object[] {"gordin.dan@gmail", "123456"},
                new object[] {"gordin.dan@gmail.com", "123456"},
                new object[] {ServiceAction.RandomEmail, "12345"},
                new object[] {"", ServiceAction.RandomPassword}
            };

        [Test, Description("Позитивный тест регистрации через popup окошко \"1000 руб\"")]
        public void CorrectPopupReg()
        {
            _mainPage = new MainPage(browser, false);
            if (_mainPage.IsOutlet()) return;
            _mainPage.PopupInput.SendKeys(ServiceAction.RandomEmail);
            _mainPage.PopupSubmit.Click();
            _mainPage.Refresh();
            StringAssert.Contains(Page.StrinFromAuthUser, _mainPage.PersonalBlockLink.Text);
        }

        [Test, Description("Негативные тесты регистрации через popup окошко (1000 руб)")]
        [TestCase("")]
        [TestCase("gordin")]
        [TestCase("gordin.dan@gmail.com")]
        public void IncorrectPopupReg(string email)
        {
            _catalogPage = new CatalogPage(browser, "/catalog", false);
            _catalogPage.PopupInput.SendKeys(email);
            _catalogPage.PopupSubmit.Click();
            _mainPage = new MainPage(browser);
            StringAssert.DoesNotContain(Page.StrinFromAuthUser, _mainPage.PersonalBlockLink.Text);
        }

        [Test, Description("Позитивный тест регистрации на промо странице ( http://brand-in-trend.ru/promo/pr13_1000/ )")]
        [TestCase("female")]
        [TestCase("male")]
        public void CorrectRegistrartionFromPromoPage1000(string genderType)
        {
            _promoPage = new PromoPage(browser, "pr13_1000");
            
            _promoPage.EmailInput.SendKeys(ServiceAction.RandomEmail);
            if (genderType == "female")
            {
                _promoPage.FemaleSubmit.Click();
            }
            else if (genderType == "male")
            {
                _promoPage.MaleSubmit.Click();
            }
            _mainPage = new MainPage(browser);
            StringAssert.Contains(Page.StrinFromAuthUser, _mainPage.PersonalBlockLink.Text);
            StringAssert.Contains(Page.StrinFromLink1000Score, _mainPage.LinkOf1000ScorePopup.Text);
        }

        [Test, Description("Негативные тесты регистрации на промо странице ( http://brand-in-trend.ru/promo/pr13_1000/ )")]
        [TestCase("")]
        [TestCase("gordin")]
        [TestCase("gordin.dan@gmail.com")]
        public void IncorrectRegistrartionFromPromoPage1000(string email)
        {
            _promoPage = new PromoPage(browser, "pr13_1000");
            _promoPage.EmailInput.SendKeys(email);
            _promoPage.FemaleSubmit.Click();
            _mainPage = new MainPage(browser);
            StringAssert.DoesNotContain(Page.StrinFromAuthUser, _mainPage.PersonalBlockLink.Text);
        }
    }

    [TestFixture]
    class CatalogTests : Test
    {
        [Test, Description("Проверка удаления товаров в всплывашке в углу экрана (сверху-справа)")]
        public void CheckDeleteProductFromCartOnPopupBlock()
        {
            _catalogPage = new CatalogPage(browser, "/new/women");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC);
            _productPage = new ProductPage(browser, _catalogPage.GetFirstProductHref());
            _productPage.BuyButtonClick();
            _productPage.DeleteProductFromCart();
            Assert.True(_productPage.CountOfProductInCart() == 0);
        }

        [Test, Description("Тест проверки фильтрации цены в каталоге (там где указывается диапазон цен)")]
        public void CheckPriceFilter()
        {
            _catalogPage = new CatalogPage(browser);
            _catalogPage.FilterBlock.FilterByPrice();
            Assert.True(IsProductsHasSamePrice());
        }

        [Test, Description("Тест проверки корректной работы 'хлебных крошек' в каталоге. Проверяет, действительно ли ссылка страницы соответствует крошкам на странице товара")]
        public void CorrectBreadCrumbs()
        {
            _catalogPage = new CatalogPage(browser);
            Assert.True(
                browser.Url ==
                browser.FindElement(By.ClassName("breadcrumbs")).FindElements(By.TagName("a")).Last().GetAttribute("href"));
        }

        [Test, Description("Тест проверки сортировки цены по возрастанию в каталоге")]
        public void CheckSortByPriceAsc()
        {
            _catalogPage = new CatalogPage(browser, "/catalog");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.ASC);
            Assert.True(IsProductsHasSortPriceBy(CommonLayer.SortType.ASC));
        }

        [Test, Description("Тест проверки сортировки цены по убыванию в каталоге")]
        public void CheckSortByPriceDesc()
        {
            _catalogPage = new CatalogPage(browser, "/catalog");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC);
            Assert.True(IsProductsHasSortPriceBy(CommonLayer.SortType.DESC));
        }

        [Test, Description("Тест проверки сортировки по популярности (убывание)")]
        public void CheckSortByPopular()
        {
            _catalogPage = new CatalogPage(browser, "/catalog");
            _catalogPage.HeaderBlock.SignIn("dgordin@brand-in-trend.ru", "77753360");
            _catalogPage.SortBlock.SortByPopular();
            Assert.True(IsProductsHasSortByPopular());
        }

        [Test, Description("Проверка фильтрации товаров по бренду в каталоге")]
        public void CheckFilterBrand()
        {
            _catalogPage = new CatalogPage(browser, "/catalog");
            _catalogPage.FilterBlock.FilterByBrand();
            Assert.True(IsProductsHasFilterBy(CommonLayer.FilterType.Brand));
        }

        [Test, Description("Проверка переключения страниц в каталоге (1, 2, 3...)")]
        public void CheckSwitchPage()
        {
            _catalogPage = new CatalogPage(browser, "/catalog/");
            var result = _catalogPage.ChangePage("one");
            Assert.True(IsPageHasSameProductsFromLastPage(result));
        }

        [Test, Description("Проверка корректного поиска, используя слово 'платье' (поиск производится в описании товара, помимо его названия)")]
        public void CheckCorrectFind()
        {
            _catalogPage = new CatalogPage(browser);
            _catalogPage.FindItems("TAYLOR");
            Assert.True(_catalogPage.GetCountProductItemsOnPage() > 0);
        }

        [Test, Description("Негативнй тест поиска под анонимом, используя заведомо неправильное слово и заодно проверка регистрации в инпуте, при пустом поиске")]
        public void CheckIncorrectFindWithAnon()
        {
            _catalogPage = new CatalogPage(browser);
            _catalogPage.FindItems("egegrtghhtr");
            StringAssert.Contains(Page.NotFoundProductItemsMessage, browser.FindElement(By.ClassName("catalog")).FindElement(By.TagName("p")).Text);
            _catalogPage.Mail.SendKeys(ServiceAction.RandomEmail);
            _catalogPage.ClickToOKOnFindPage();
            StringAssert.Contains(Page.StrinFromAuthUser, _catalogPage.PersonalBlockLink.Text);
        }

        [Test, Description("Негативный тест поиска под юзером")]
        public void CheckIncorrectFindWithUser()
        {
            _catalogPage = new CatalogPage(browser);
            _catalogPage.HeaderBlock.SignIn("dgordin@brand-in-trend.ru", "77753360");
            _catalogPage.ClosePopupWindow(CommonLayer.PlatformMethod.Selenium);
            _catalogPage.FindItems("egegrtghhtr");
            Assert.True(_catalogPage.GetCountProductItemsOnPage() == 0);
            StringAssert.Contains(Page.NotFoundProductItemsMessage, browser.FindElement(By.ClassName("catalog")).FindElement(By.TagName("p")).Text);
        }
    }

    [TestFixture]
    class ProductTests : Test
    {
        [Test, Description("Проверка добавления товара в корзину")]
        public void AddToCart()
        {
            _productPage = new ProductPage(browser, true);
            _productPage.BuyButtonClick();
            _productPage.WaitForJQuery();
            Assert.True(Regex.IsMatch(_productPage.Cartlink.Text, @"\d+"));
        }

        [Test, Description("Провекра добавления товара в \"Избранное\" (лайк, сердечко)")]
        public void AddToFavorite()
        {
            _productPage = new ProductPage(browser, true);
            var likeCountBefore = (int) Convert.ToUInt32(_productPage.Heart.Text);
            _productPage.JsAction("$('#heart').mouseover()");
            _productPage.FindElement(browser, By.Id("heart"), 15).Click();
            _productPage.WaitForJQuery();
            var likeCountAfter = (int) Convert.ToUInt32(_productPage.FindElement(browser, By.Id("heart")).Text);
            Assert.True(likeCountAfter - likeCountBefore == 1);
        }
    }

    [TestFixture]
    class CartTests : Test
    {
        [Test, Description("Проверяем будет ли Пикпоинт, если в составе заказа будут сапоги")]
        public void CheckPickPointRestriction()
        {
            _catalogPage = new CatalogPage(browser, "/catalog/women/shoe/high_boots/");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC);
            if (!_catalogPage.IsCatalogEmpty())
            {
                _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
                _cartPage.ClickTo2Button();
                _cartPage.AuthInCart("dgordin@brand-in-trend.ru", "77753360", "rightBlock");
                _cartPage.FastSelectMsk.Click();
                _cartPage.ClickTo3Button();
                Assert.True(!_cartPage.IsDevireryExist("pickpoint"));
            }
            else
            {
                throw new Exception("Каталог пуст");
            }
        }

        [Test, Description("Проверяем будет ли DHL, если в составе заказа будет сумка")]
        public void CheckDhlRestriction()
        {
            _catalogPage = new CatalogPage(browser, "/catalog/women/bags/");
            if (!_catalogPage.IsCatalogEmpty())
            {
                _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
                _cartPage.ClickTo2Button();
                _cartPage.AuthInCart("dgordin@brand-in-trend.ru", "77753360", "leftBlock");
                _cartPage.FastSelectMsk.Click();
                _cartPage.ClickTo3Button();
                Assert.False(_cartPage.IsDevireryExist("dhl"));
            }
        }

        [Test, Description("Позитивный тест проверки изменения количества товара в корзине")]
        public void CheckChangeCountOfProduct()
        {
            _productPage = new ProductPage(browser, ServiceAction.Host + "/catalog/id" + ORM.GetProductWithCountMore1Item(), true);
            _productPage.BuyButtonClick();
            _cartPage = new CartPage(browser);
            _cartPage.ChangeCountOfProduct(1, "plus");
            Assert.True(_cartPage.GetCountOfProduct(1) == 2);
            _cartPage.ChangeCountOfProduct(1, "minus");
            Assert.True(_cartPage.GetCountOfProduct(1) == 1);
        }

        [Test, Description("Позитивный тест проверки добавления другого размера товара в корзине")]
        public void CheckAddOtherSize()
        {
            _catalogPage = new CatalogPage(browser, "/catalog/women/clothing/");
            _productPage = _catalogPage.GetProductWithFewSize();
            _cartPage = AddProductToCart(_productPage.GetHostUrl());
            int countOfProductsBefore = _cartPage.GetProductsCount("cart");
            _cartPage.AddOtherSize(1, 1);
            int countOfProductsAfter = _cartPage.GetProductsCount("cart");
            Debug.WriteLine(countOfProductsBefore);
            Debug.WriteLine(countOfProductsAfter);
            Assert.True(countOfProductsAfter > countOfProductsBefore);
        }

        [Test, Description("Позитивный тест удаления товара из Корзины")]
        public void CheckDeleteProductInCart()
        {
            _catalogPage = new CatalogPage(browser);
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
            int countOfProductsBefore = _cartPage.GetProductsCount("cart");
            _cartPage.ClickToX(1);
            _cartPage.ClickToKill(1);
            int countOfProductsAfter = _cartPage.GetProductsCount("cart");
            Assert.True(countOfProductsBefore - countOfProductsAfter == 1);
        }

        [Test, Description("Тест проверки добавления товара в \"Отложенные\"")]
        public void CheckThrowProductToDelayInCart()
        {
            _catalogPage = new CatalogPage(browser);
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
            _cartPage.ClickToDelay(1);
            Assert.True(_cartPage.GetDelayProductsCountInFirstPlace() == 1);
            Assert.True(_cartPage.GetDelayProductsCountInSecondPlace() == 1);
        }

        [Test, Description("Тест проверки добавлния товара из Отложенные в Корзину")]
        public void CheckGetFromDelayProductInCart()
        {
            _catalogPage = new CatalogPage(browser, "/catalog/women/clothing/");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC); //сортируем по цене desc на случай, если первый товар продан (дорогое наврядли будет продано)
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
            _cartPage.ClickToDelay(1);
            _cartPage.OpenDelayWindow();
            _cartPage.AddDelayProductInOrder(1);
            _cartPage.CloseDelayWindow();
            Assert.True(_cartPage.GetProductsCount("cart") == 1);
        }
        
        [Test, Description("Проверка лимита скидки в 60%. Для теста нужно чтобы скидка у товара была минимум 50% и VIP-скидка пользователя была минимум 30%")]
        public void CheckMaxDiscountLimit() 
        {
            return; //@todo: нужно рефакторить, т.к. нужно постоянно выбирать товар с 50% скидкой, а достоверно такой товар взять не получается
            _mainPage = new MainPage(browser);
            _mainPage.HeaderElement.SignIn("dgordin@brand-in-trend.ru", "77753360");
            _cartPage = new CartPage(browser);
            _cartPage.ClearCart();
            _catalogPage = new CatalogPage(browser, "/sale/women/");
            _catalogPage.CloseSuccessAuthWindow();
            _catalogPage.SetDiscountValueInFilter("50-70");
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
            Assert.True(_cartPage.GetLastDiscoutPercent() == 60);
        }

        [Test, Description("Тест проверки работы скидки у VIP'ов")]
        public void CheckVipOrder()
        {
            _catalogPage = new CatalogPage(browser, "/new/women/");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC);
            _catalogPage.HeaderBlock.SignIn("dgordin@brand-in-trend.ru", "77753360");
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref()); // берем продукт без скидок
            Assert.True(_cartPage.GetLastDiscoutPercent() >= 10); // VIP-скидки идут от 10% и далее
        }

        [Test, Description("Проверка работы скидки, при выборе способа оплаты - PayOnline")]
        public void CheckPayonlineDiscount()
        {
            _cartPage = AddProductToCart();
            _cartPage.ClickTo2Button();
            _cartPage.AuthInCart("gordan.ru@gmail.com", "77753360", "leftBlock");
            _cartPage.ClickTo3Button();
            int summPriceBefore = _cartPage.GetCurrentPrice();
            _cartPage.ClickPayonlineButton();
            _cartPage.ClickTo4Button();
            Thread.Sleep(5000);
            int summPriceAfter = _cartPage.GetCurrentPrice();
            Debug.WriteLine("summPriceBefore: " + summPriceBefore);
            Debug.WriteLine("summPriceAfter: " + summPriceAfter);
            Assert.True(summPriceBefore > summPriceAfter);
        }

        [Test, Description("")]
        public void CheckFirstRegistrationDiscountOnMainPage()
        {
            _mainPage = new MainPage(browser);
            _mainPage.HeaderElement.SignUp(ServiceAction.RandomEmail, ServiceAction.RandomPassword);
            if (!_mainPage.IsOutlet())
            {
                StringAssert.Contains(Page.StrinFromLink1000Score, _mainPage.LinkOf1000ScorePopup.Text);
            }
            _catalogPage = new CatalogPage(browser, "/catalog");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC);
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());          
             _regex = new Regex(@"\D+");
            int summBeforeDiscount = Convert.ToInt32(_regex.Replace(_cartPage.PriceOfFirstProductInCart.Text, ""));
            int summAfterDiscount = Convert.ToInt32(_regex.Replace(_cartPage.TotalPrice.Text, ""));
            Assert.True(summBeforeDiscount > summAfterDiscount);
        }

        [Test]
        public void CheckFirstRegistrationDiscountOn()
        {
            _productPage = new ProductPage(browser, true);
            _productPage.CreateFastOrder();
            _productPage.Refresh();
            _catalogPage = new CatalogPage(browser, "/catalog/");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC);
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
            _regex = new Regex(@"\D+");
            int summBeforeDiscount = Convert.ToInt32(_regex.Replace(_cartPage.PriceOfFirstProductInCart.Text, ""));
            int summAfterDiscount = Convert.ToInt32(_regex.Replace(_cartPage.TotalPrice.Text, ""));
            Debug.WriteLine(summBeforeDiscount);
            Debug.WriteLine(summAfterDiscount);
            Assert.True(summBeforeDiscount > summAfterDiscount);
        }

        [Test]
        public void CheckFirstRegistrationDiscountOnCartPage()
        {
            _cartPage = AddProductToCart();
            _cartPage.ClickTo2Button();
            _cartPage.Registration();
            _cartPage.ClickTo3Button();
            _cartPage.ClickTo4Button();
            _cartPage.ClickToFinishButton();
            _mainPage = new MainPage(browser);
            StringAssert.Contains(Page.StrinFromAuthUser, _mainPage.PersonalBlockLink.Text);
            if (!_mainPage.IsOutlet())
            {
                StringAssert.Contains(Page.StrinFromLink1000Score, _mainPage.LinkOf1000ScorePopup.Text);
            }
            _catalogPage = new CatalogPage(browser, "/catalog/");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC);
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
             _regex = new Regex(@"\D+");
            int summBeforeDiscount = Convert.ToInt32(_regex.Replace(_cartPage.PriceOfFirstProductInCart.Text, ""));
            int summAfterDiscount = Convert.ToInt32(_regex.Replace(_cartPage.TotalPrice.Text, ""));
            Assert.True(summBeforeDiscount > summAfterDiscount);
        }

        [Test, Description("Тест проверки платной доставки на основном сайте (DHL)")]
        public void CheckPayForDelivery()
        {
            _catalogPage = new CatalogPage(browser, "/catalog/women/clothing/w_shirts/");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.ASC);
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
            _cartPage.ClickTo2Button();
            _cartPage.Registration();
            _cartPage.ClickTo3Button();
            int summPriceBefore = _cartPage.GetCurrentPrice();
            if (summPriceBefore > 5000)
                throw new Exception("Неподходящие условия для выполнения теста. Сумма корзины больше 5000 руб");
            _cartPage.ClickDhlButton();
            int summPriceAfter = _cartPage.GetCurrentPrice();
            Assert.True(summPriceBefore < summPriceAfter);
        }

        [Test]
        public void CheckOrderWithPromoCode()
        {
            _catalogPage = new CatalogPage(browser, "/catalog/women/clothing/");
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC); //сортируем по цене desc на случай, если первый товар продан (дорогое наврядли будет продано)
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref()); // Добавляем товар из Новинок, т.к. там скорее всего нет скидок (для чистоты эксперимента)
            int summPriceBefore = _cartPage.GetCurrentPrice("totalPrice");
            _cartPage.InputPromoCode("promo10");
            int summPriceAfter = _cartPage.GetCurrentPrice("totalPrice");
            Assert.True(summPriceBefore > summPriceAfter);
            Assert.True(_cartPage.GetFirstDiscoutPercent() == 10);
        }

        [Test, Description("Негативный тест проверки регистрации в Корзине")]
        [TestCase("gordin")]
        [TestCase("gordin@")]
        [TestCase("gordin@gmail")]
        [TestCase("gordin.dan@gmail.com")]
        public void CheckIncorrectRegOnCartPage(string email)
        {
            _catalogPage = new CatalogPage(browser);
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC); ;
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
            _cartPage.ClickTo2Button();
            _cartPage.Registration(email);
            _cartPage.ClickTo3Button();
            Assert.False(_catalogPage.FindElement(browser, By.ClassName("goto_confirm")).Displayed);
        }

        [Test, Description("Негативный тест проверки ввода телефона в Корзине")]
        [TestCaseSource("DivideCasesForIncorrectInputPhoneInCart")]
        public void CheckIncorrectInputPhoneInCartPage(string email, string phone)
        {
            _catalogPage = new CatalogPage(browser);
            _catalogPage.SortBlock.SortByPrice(CommonLayer.SortType.DESC);
            _cartPage = AddProductToCart(_catalogPage.GetFirstProductHref());
            _cartPage.ClickTo2Button();
            _cartPage.Registration(email, phone);
            _cartPage.ClickTo3Button();
            Assert.False(_catalogPage.FindElement(browser, By.ClassName("goto_confirm")).Displayed);
        }

        private static readonly object[] DivideCasesForIncorrectInputPhoneInCart =
            {
                new object[] {ServiceAction.RandomEmail, ""},
                new object[] {ServiceAction.RandomEmail, " "}
            };

        private CartPage AddProductToCart()
        {
            _productPage = new ProductPage(browser, true);
            _productPage.BuyButtonClick();
            return new CartPage(browser);
        }

        private CartPage AddProductToCart(string url)
        {
            _productPage = new ProductPage(browser, url);
            _productPage.BuyButtonClick();
            return new CartPage(browser);
        }
    }

    [TestFixture]
    class CommonTests : Test
    {
        [Test, Description("Положительный тест проверки отображения социального попапа")]
        public void CheckSocialPopupExist()
        {
            _catalogPage = new CatalogPage(browser);
            _catalogPage.Refresh();
            _catalogPage.Refresh();
            _catalogPage.Refresh();
            Assert.True(_catalogPage.IsElementPresent(By.Id("socialnetwork")));
        }

        [Test, Description("Положительный тест проверки отображения попапа регистрации")]
        public void CheckRegistrationPopupExist()
        {
            _catalogPage = new CatalogPage(browser, popupClose:false);
            Assert.True(_catalogPage.IsElementPresent(By.Id("promoform3")));
        }
    }
}